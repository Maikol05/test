package prueba1.models;

import java.util.Date;

public class CustomerPurchase {
	
	private String details;
	private Date date;
	private Long id;
	
public CustomerPurchase() {
	}
	
	public CustomerPurchase(String details, Date date, Long id) {
		
		this.details = details;
		this.date = date;
		this.id = id;
	}


	public String getDetails() {
		return details;
	}


	public void setDetails(String details) {
		this.details = details;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "CustomerPurchase [details=" + details + ", date=" + date + ", id=" + id + "]";
	}
	
}
