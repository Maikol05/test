package prueba1.models;

public class Customer{
	
	private String name;
	private String code;
	private String address;
	private Long id;
	
public Customer() {
		
	}
	
	public Customer(String name, String code, String address, Long id) {
		
		this.name = name;
		this.code = code;
		this.address = address;
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", code=" + code + ", address=" + address + ", id=" + id + "]";
	}

}
