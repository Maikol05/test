package prueba1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.protocol.Resultset;

import prueba1.conection.Conection;
import prueba1.models.Customer;

public class CustomerDao {
	
	private Connection connection;
	private PreparedStatement statement;
	private boolean estadoOperacion;
	
	//guardar
	public boolean save(Customer customer) throws SQLException {
		String sql = null;
		estadoOperacion = false;
		connection = obtenerConexion();
		
		try {
			connection.setAutoCommit(false);
			sql="INSERT INTO customer(adress,code,name) VALUES(?,?,?);";
			statement=connection.prepareStatement(sql);
			
			statement.setString(1, customer.getAddress());
			statement.setString(2, customer.getCode());
			statement.setString(3, customer.getName());
			
			estadoOperacion = statement.executeUpdate()>0;
			connection.commit();
			statement.close();
			connection.close();
			
		} catch (SQLException e) {
			connection.rollback();
			e.printStackTrace();
		}
		
		return estadoOperacion;
	}
	
	//editar
		public boolean edit(Customer customer) throws SQLException {
			String sql=null;
			estadoOperacion=false;
			connection=obtenerConexion();
			
			try {
				connection.setAutoCommit(false);
				sql="UPDATE customer SET name=?, adress=?, code=? WHERE id=?";
				statement=connection.prepareStatement(sql);
				
				statement.setString(2, customer.getAddress());
				statement.setString(3, customer.getCode());
				statement.setString(1, customer.getName());
				statement.setLong(4, customer.getId());
				
				estadoOperacion = statement.executeUpdate()>0;
				connection.commit();
				statement.close();
				
				connection.close();
			} catch (SQLException e) {
				connection.rollback();
				e.printStackTrace();
			}
			
			return estadoOperacion;
		}
		
	//eliminar
		public boolean delate(Long idCustomer) throws SQLException {
			String sql=null;
			estadoOperacion=false;
			connection=obtenerConexion();
			
			try {
				
				connection.setAutoCommit(false);
				sql="DELETE FROM customer WHERE id=?";
				statement=connection.prepareStatement(sql);
				
				statement.setLong(1, idCustomer);
				
				estadoOperacion = statement.executeUpdate()>0;
				connection.commit();
				statement.close();
				
				connection.close();
				
			} catch (SQLException e) {
				connection.rollback();
				e.printStackTrace();
			}
			
			return estadoOperacion;
		}
		//obtener lista de clientes
		public List<Customer> obtain() throws SQLException {
			
			ResultSet resultSet = null;
			List<Customer> listaCustomer = new ArrayList<>();
			
			String sql=null;
			estadoOperacion=false;
			connection=obtenerConexion();
			
			try {
				
				
				sql="SELECT * FROM customer";
				resultSet=statement.executeQuery(sql);
				
				while(resultSet.next()) {
					Customer p=new Customer();
					p.setAddress(resultSet.getString(1));
					p.setCode(resultSet.getString(2));
					p.setName(resultSet.getString(3));					
					p.setId(resultSet.getLong(4));
					
					listaCustomer.add(p);
					
				}
				
			} catch (SQLException e) {
				connection.rollback();
				e.printStackTrace();
			}
			
			return listaCustomer;
		}
		
		//obtener cliente
		public Customer obtain( Long idCustomer) {
			return null;
		}
		
		//obtener conecion
		private Connection obtenerConexion() throws SQLException {
			return Conection.getConnection();
		}
				

}
